# soal-shift-sisop-modul-4-D04-2022
# Soal Shift Sistem Operasi Modul 4 Kelompok D04 2022

## Anggota Kelompok

<table>
    <tr>
        <th>NRP</th>
        <th>Nama</th>
    </tr>
    <tr>
        <td>Muhammad Lintang Panjerino</td>
        <td>5025201045</td>
    </tr>
    <tr>
        <td>Muhammad Yunus</td>
        <td>5025201171</td>
    </tr>
    <tr>
        <td>PEDRO T KORWA</td>
        <td>05111940007003</td>
    </tr>
<table>


#### Fungsi Utilitas:
```c
int split_ext_id(char *path)
{
  int flag = 0;
  for (int i = strlen(path) - 1; i >= 0; i--)
  {
    if (path[i] == '.')
    {
      if (flag == 1)
        return i;
      else
        flag = 1;
    }
  }
  return strlen(path);
}
```
//
```c
int ext_id(char *path)
{
  for (int i = strlen(path) - 1; i >= 0; i--)
  {
    if (path[i] == '.')
      return i;
  }
  return strlen(path);
}
```
Fungsi split_ext_id() dan ext_id() digunakan untuk memisahkan ekstensi dengan nama filenya, fungsi split_ext_id dan ext_id akan mengembalikan nilai int berupa posisi titik "." ekstensi sebuah file untuk dimanfaatkan sebagai penanda bahwa nama setelah titik merupakan ekstensi/nama file.

//
```c
int slash_id(char *path, int hasil)
{
  for (int i = 0; i < strlen(path); i++)
  {
    if (path[i] == '/')
      return i + 1;
  }
  return hasil;
} 
```

Fungsi slash_id() berfungsi mengembalikan nilai int berupa letak slash "/" berada pada path. Fungsi ini dimanfaatkan dalam memisahkan tiap-tiap folder dan file dari folder utama. Karena folder utama nantinya tidak akan dienkripsi/didekripsi tetapi folder/file didalam folder utama lah yang akan dienkripsi/didekripsi maka nama-nama file/folder tersebut harus dipisahkan berdasarkan path yang d-*ipassing* ke fungsi.

### Variabel GLobal

```c
// variabel global
char  dirpath[maxSize];
char *key_for_vigenere = "INNUGANTENG";
char *animeku = "Animeku_";
char *ian = "IAN_";
char *nds = "nam_do-saq_";
```
Variabel global yang digunakan dalam pengerjaan soal. Dalam hal ini merupakan nama-nama folder spesial.


## 1. Soal 1
#### Deskripsi Soal:
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:

### 1.A
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
Contoh :
“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”

### 1.B
Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

### 1.C
Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

### 1.D
Setiap data yang terencode akan masuk dalam file “Wibu.log”
Contoh isi:

RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat

RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

### 1.E
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

Note : filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root) filesystem adalah directory **/home/[USER]/Documents**, dalam penamaan file ‘/’ diabaikan, dan ekstensi tidak perlu di-encode

#### Pembahasan

Fungsi wibu_log
```c
void wibu_log(char *nama, const char *from, const char *to)
{
  char wibuLogMsg[maxSize];

  FILE *file;
  file = fopen("/home/lintang/wibu.log", "a");
  sprintf(wibuLogMsg, "%s %s/%s --> %s/%s\n", nama, dirpath, from, dirpath, to);

  fputs(wibuLogMsg, file);
  fclose(file);
  return;
}
```
Fungsi wibu_log() berjalan Ketika suatu folder di rename menjadi Animeku_ maka file log akan menampilkan log berupa nama folder/file dalam folder(Animeku_) tersebut yang mengalami enkrisi. Begitu pula sebaliknya ketika suatu folder di-rename dari Animeku_ menjadi nama folder lain, maka isi folder Animeku_ itu akan terdecode/dekripsi. Fortmat output disesuaikan dengan soal.

Fungsi Enkripsi
```c
void atbash_x_rot13_encrypt_func(char *filename)
{
  if (!strcmp(filename, ".") || !strcmp(filename, ".."))
    return;

  int endID = split_ext_id(filename);
  if (endID == strlen(filename))
    endID = ext_id(filename);
  int startID = slash_id(filename, 0);

  char file_origin[maxSize];
  for (int i = startID; i < endID; ++i)
  {
    file_origin[i] = filename[i];
    if (filename[i] != '/' && isalpha(filename[i]))
    {
      if (filename[i] >= 'A' && filename[i] <= 'Z')
      {
        if (!((filename[i] >= 0 && filename[i] < 65) || (filename[i] > 90 && filename[i] < 97) || (filename[i] > 122 && filename[i] <= 127)))
        {
          if (filename[i] >= 'A' && filename[i] <= 'Z')
            filename[i] = 'Z' + 'A' - filename[i];
          if (filename[i] >= 'a' && filename[i] <= 'z')
            filename[i] = 'z' + 'a' - filename[i];
        }
      }

      else
      {
        if ((filename[i] >= 97 && filename[i] <= 122) || (filename[i] >= 65 && filename[i] <= 90))
        {
          if (filename[i] > 109 || (filename[i] > 77 && filename[i] < 91))
            filename[i] -= 13;
          else
            filename[i] += 13;
        }
      }
    }
  }
  wibu_log("RENAME terenkripsi", file_origin, filename);
}
```

Fungsi Dekripsi
```c
void atbash_x_rot13_decrypt_func(char *filename)
{
  if (!strcmp(filename, ".") || !strcmp(filename, ".."))
    return;

  int endID = split_ext_id(filename);
  if (endID == strlen(filename))
    endID = ext_id(filename);
  int startID = slash_id(filename, endID);

  char file_origin[maxSize];
  for (int i = startID; i < endID; ++i)
  {
    file_origin[i] = filename[i];
    if (filename[i] != '/' && isalpha(filename[i]))
    {
      if (filename[i] >= 'A' && filename[i] <= 'Z')
      {
        if (!((filename[i] >= 0 && filename[i] < 65) || (filename[i] > 90 && filename[i] < 97) || (filename[i] > 122 && filename[i] <= 127)))
        {
          if (filename[i] >= 'A' && filename[i] <= 'Z')
            filename[i] = 'Z' + 'A' - filename[i];
          if (filename[i] >= 'a' && filename[i] <= 'z')
            filename[i] = 'z' + 'a' - filename[i];
        }
      }

      else
      {
        if ((filename[i] >= 97 && filename[i] <= 122) || (filename[i] >= 65 && filename[i] <= 90))
        {
          if (filename[i] > 109 || (filename[i] > 77 && filename[i] < 91))
            filename[i] -= 13;
          else
            filename[i] += 13;
        }
      }
    }
  }
  wibu_log("RENAME terdecode", file_origin, filename);
}
```

Fungsi atbash_x_rot13_encrypt_func() dan atbash_x_rot13_decrypt_func() berfungsi untuk melakukan enkripsi dan dekripsi terhadap nama file/folder dari path/file yang di-passing ke fungsi.

Fungsi akan melakukan pemisahan nama file dan ekstesnsinya dengan fungsi utlitas ext_id() dan split_ext_id() yang sudah dijelaskan sebelumnya.

Selanjutnya semua folder didalam folder utama (folder pertama) akan di enkripsi/dekripsi oleh karena itu dilakukan fungsi slash_id untuk mendapatkan semua nama file/folder yang akan di enkrip/dekrip selain folder utama.

Semua nama file/folder didalam folder utama akan mengalami enkripsi/dekripsi, untuk huruf besar akan mengunakan enkripsi/dekripsi algoritma **Atbash Cipher**, sedangkan huruf kecil akan mengunakan algortima enkripsi/dekripsi **ROT13**.

Terakhir fungsi akan memanggil fungsi wibu_log() untuk menampilakn log atau aktivitas yang terjadi pada folder/file yang di enkripsi/dekripsi.


## 2. Soal 2
#### Deskripsi Soal:
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut :

### 2.A
Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

### 2.B
Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.

### 2.C
Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

### 2.D
Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.

### 2.E
1.  Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut :
  
[Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]

Keterangan :

Level : Level logging
dd : Tanggal
mm : Bulan
yyyy : Tahun
HH : Jam (dengan format 24 Jam)
MM : Menit
SS : Detik
CMD : System call yang terpanggil
DESC : Informasi dan parameter tambahan

Contoh :

WARNING::21042022-12:02:42::RMDIR::/RichBrian
INFO::21042022-12:01:20::RENAME::/Song::/IAN_Song
INFO::21042022-12:06:41::MKDIR::/BillieEilish

NOTE :
Misalkan terdapat file bernama idontwannabeyouanymore.txt pada direktori BillieEilish.

“IAN_Song/BillieEilish/idontwannabeyouanymore.txt” → “IAN_Song/JvyfoeRbpvyp/qqbhzwngrnhmlbognlfsek.bkg”
  
Referensi : [https://www.dcode.fr/vigenere-cipher](https://www.dcode.fr/vigenere-cipher)

### Pembahasan
Vignere Encrypt
```C
void vigenere_encrypt_func(char *src)
{
  if (!strcmp(src, ".") || !strcmp(src, ".."))
    return;
  int endID = split_ext_id(src);
  int startID = slash_id(src, 0);

  for (int i = startID; i < endID; i++)
  {
    if (src[i] != '/' && isalpha(src[i]))
    {
      char temp1 = src[i];
      char temp2 = key_for_vigenere[(i - startID) % strlen(key_for_vigenere)];
      if (isupper(src[i]))
      {
        temp1 -= 'A';
        temp2 -= 'A';
        temp1 = (temp1 + temp2) % 26;
        temp1 += 'A';
      }
      else
      {
        temp1 -= 'a';
        temp2 = tolower(temp2) - 'a';
        temp1 = (temp1 + temp2) % 26;
        temp1 += 'a';
      }
      src[i] = temp1;
    }
  }
}
```

Vignere Decrypt
```c
void vigenere_decrypt_func(char *src)
{
  if (!strcmp(src, ".") || !strcmp(src, ".."))
    return;

  int endID = split_ext_id(src);
  int startID = slash_id(src, endID);

  for (int i = startID; i < endID; i++)
  {
    if (src[i] != '/' && isalpha(src[i]))
    {
      char temp1 = src[i];
      char temp2 = key_for_vigenere[(i - startID) % strlen(key_for_vigenere)];
      if (isupper(src[i]))
      {
        temp1 -= 'A';
        temp2 -= 'A';
        temp1 = (temp1 - temp2 + 26) % 26; // Vigenere cipher
        temp1 += 'A';
      }
      else
      {
        temp1 -= 'a';
        temp2 = tolower(temp2) - 'a';
        temp1 = (temp1 - temp2 + 26) % 26; // Vigenere cipher
        temp1 += 'a';
      }
      src[i] = temp1;
    }
  }
}
```

Fugnsi vignere_encrypt()/decrypt_func() akan melakukan enkripsi nama folder dan file secara terpisah relative terhadap folder utama.

Fungsi akan mendapatkan path folder utama beserta isinya (ex. fileUtama/foderEncrypt/fileEncrypt.txt), selanjutnya memisahkan folder/file yang akan di-encrypt/di-decrypt dari folder utama karena folder utama tidak akan di-encrypt/di-decrypt, file juga akan dipisahkan dengan ekstensinya untuk di-encrypt/di-decrypt secara terpisah.

Selanjutnya algoritma vignere cipher baik untuk encrypt/decrypt dijalankan terhadap nama folder, file, dan extensi yang berda pada folder utama.

//Fungsi Log
```c
void hayolongapain_d04_log(char *nama, char *fpath)
{
  // [Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]
  //   Contoh :
  // WARNING::21042022-12:02:42::RMDIR::/RichBrian
  // INFO::21042022-12:01:20::RENAME::/Song::/IAN_Song
  // INFO::21042022-12:06:41::MKDIR::/BillieEilish

  time_t rawtime;
  struct tm *timeinfo;
  time(&rawtime);
  timeinfo = localtime(&rawtime);

  char hayolongapainLogMsg[maxSize];

  FILE *file;
  file = fopen("/home/lintang/hayolongapain_d04.log", "a");

  if (strcmp(nama, "UNLINK") == 0 || strcmp(nama, "RMDIR") == 0)
    sprintf(hayolongapainLogMsg, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, fpath);
  else
    sprintf(hayolongapainLogMsg, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, fpath);

  fputs(hayolongapainLogMsg, file);
  fclose(file);
  return;
}
```
```c
void hayolongapain_d04_log_b(char *nama, const char *from, const char *to)
{
  time_t rawtime;
  struct tm *timeinfo;
  time(&rawtime);
  timeinfo = localtime(&rawtime);

  char hayolongapainLogMsg[maxSize];

  FILE *file;
  file = fopen("/home/lintang/hayolongapain_d04.log", "a");

  if (strcmp(nama, "UNLINK") == 0 || strcmp(nama, "RMDIR") == 0)
    sprintf(hayolongapainLogMsg, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, from, to);
  else
    sprintf(hayolongapainLogMsg, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", timeinfo->tm_mday, timeinfo->tm_mon + 1, timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, nama, from, to);

  fputs(hayolongapainLogMsg, file);
  fclose(file);
  return;
}
```
Fungsi log digunakan untuk mencatat semua aktivitas yang terjadi di dalam folder utama, aktivitas dibagi menjadi 2 yaitu "INFO" dan "WARNING",

WARNING untuk aktivitas rmdir dan unlink sedangkan INFO untuk aktivitas selain itu.

- hayolongapain_d04_log fungsi log yang hanya memiliki 1 parameter contohnya unlink
- hayolongapain_d04_log_b untuk log aktivitas yang memiliki 2 contohnya rename (name_sebelum dan nama_sesudah)

Fungsi akan menuliskan sesuai format yang diminta soal, pertama menetukan waktu aktivitas dijalankan dengan memanfaat library time time.h. hasil yang telah didaptakan dicetak pada file hayolongapain_d04.log.
Selanjutnya menentukan level dari aktivitas jika merupakan unlink/rmdir maka merupakan level warning selain itu merupakan level info.

Setelah format log telah dibentuk, maka format log tersebut disimpan/ditambahkan pada file hayolongapain_d04.log.


### Fungsi-Fungsi Operasi FUSE
```c
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,

    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .open = xmp_open,
    .write = xmp_write,
};
```

Struktur fuse yang kami gunakan adalah seperti diatas dan berikut penjelasannya:

getattr = Untuk mengembalikan attribute dari suatu file  
readdir = Untuk melakukan pembacaan directory  
read = untuk melakukan pembacaan file  
mkdir = untuk pembuatan suatu directory baru  
rmdir = untuk menghapus directory  
rename = untuk menganti nama sebuah directory ataupun file  
truncate = perpanjang file yang diberikan sehingga ukurannya tepat beberapa byte  
write = penulisan ke suatu file atau directory  
open = membuka file  
statfs = mengembalikan statistic dari file yang digunakan  
unlink = menghapus simbolik link dan file yang diberikan  
chmod = memodifikasi izin  
chown = mengganti kepemilikan file

Setiap fungsi akan berjalan pada Fuse dengan melakukan pengecekan sebagai berikut:
```c
char *a = strstr(path, animeku);
  if (a != NULL)
    //jalankan fungsi encrypt/decrypt atbash_x_rot13
  char *b = strstr(path, ian);
  if (b != NULL)
    //jalankan fungsi encrypt/decrypt vignere;
    
// tulis ke hayolongapain_d04.log 
hayolongapain_d04_log_b("RENAME", fromDir, toDir);
```

Jenis operasi apakah encrypt atau decrypt akan ditentukan berdasarkan jenis opersi FUSE tersebut.
Beserta memanggil fungsi hayolongapain_d04_log sesuai operasi FUSE yang menjalankannya, dalam contoh operasi xmp_rename.


### Screenshot bukti jalannya program

##### Berikut adalah isi dari folder **/home/[USER]/Documents** yang merupakan mount source (root):

![](img/sebelum/ss1.png)

##### Di dalam folder **Animeku_1** terdapat 1 folder bernama **Animeku_11** dan 1 file **anya_FORGER.cpp**. Selain itu, di dalam folder **Animeku_11** terdapat 1 file bernama **lintang.txt**:

![](img/sebelum/ss2.png)
![](img/sebelum/ss3.png)

##### Setelah *fuse* dijalankan, maka semua yang ada di dalam folder **Animeku_1** terenkripsi menggunakan enkripsi **ATBASH CIPHER** (untuk uppercase) dan **ROT13** (untuk lowercase), sehingga hasilnya akan menjadi berikut:

![](img/setelah/ss1.png)
![](img/setelah/ss2.png)

##### Di dalam folder **IAN_Song** terdapat 1 folder bernama **BillieEilish** dan 1 file bernama **idontwannabeyouanymore.txt**

![](img/sebelum/ss4.png)

##### Setelah *fuse* dijalankan, maka semua yang ada di dalam folder **IAN_Song** terenkripsi menggunakan enkripsi **VIGENERE CIPHER**, sehingga hasilnya akan menjadi seperti berikut:

![](img/setelah/ss3.png)

##### Di dalam folder **nam_do-saq_1** terdapat 2 folder bernama **Animeku_123** dan **IAN_1** dan juga 1 file bernama **isHaQ_KEreN.txt**. Di dalam folder **Animeku_123** terdapat sebuah file bernama **anya_FORGER.txt**, sedangkan di dalam folder **IAN_1** terdapat sebuah file bernama **idontwannabeyouanymore.txt**

![](img/sebelum/ss5.png)
![](img/sebelum/ss6.png)
![](img/sebelum/ss7.png)

##### Setelah *fuse* dijalankan, folder **nam_do-saq_1** akan menjadi direktori spesial dimana akan mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (rekursif)., sehingga akan seperti di bawah ini:

![](img/setelah/ss4.png)
![](img/setelah/ss5.png)

##### Di dalam folder **rename**, ada sebuah file yang bernama **anya_FORGER.txt**. Folder rename nantinya akan di-rename menjadi **Animeku_rename** sebagai salah satu pembuktian fitur rename.

![](img/sebelum/ss8.png)

##### Setelah di-rename menjadi **Animeku_rename**, maka file **anya_FORGER.txt** akan terenkripsi sesuai aturan "Animeku_", sehingga hasilnya akan menjadi seperti berikut:

![](img/setelah/ss6.png)

##### Di dalam folder **buat_rmdir** terdapat sebuah file bernama **buat_unlink.c**. Folder **buat_rmdir** dan file **buat_unlink.c** ini nantinya akan dihapus, kemudian akan memunculkan log **WARNING** ke **hayolongapain_d04.log**

![](img/sebelum/ss9.png)

##### Berikut adalah isi folder utama (root) setelah folder **buat_rmdir** dan file **buat_unlink.c** dihapus:

![](img/setelah/ss7.png)

##### Berikut adalah contoh isi dari file **hayolongapain_d04.log** dan **Wibu.log**

![](img/setelah/ss8.png)
![](img/setelah/ss9.png)
